import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  body {
    background: #FFFFFF;
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font-family: 'Montserrat', sans-serif;
    font-weight: normal;
    color: var(--secundary);
  }

  html {
    /* 1rem = 10px */
    font-size: 62.5%;
  }

  #root {
    display: flex;
    justify-content: center;
    max-width: 1360px;
    width: 100vw;
    margin: 0 auto;
  }

  :root {
    --primary: #FFF;
    --secundary: #000;
    --boxInfo: #333333;
    --green: #41FFB4;
    --box-background: #E5E5E5;
  }

  button {
    cursor: pointer;
  }

`;
