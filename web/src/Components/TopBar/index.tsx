import React from 'react';
import {
  FiMenu,
  FiSearch,
  FiUser,
  FiShoppingCart,
  FiPower,
} from 'react-icons/fi';
import { Link } from 'react-router-dom';

import { Container, FirstItems, Form } from './styles';

const TopBar: React.FC = () => {
  return (
    <Container>
      <FirstItems>
        <button type="button">
          <FiMenu />
        </button>
        <Link to="" className="menuToggle">
          <FiMenu />
        </Link>
        <strong className="yourShoes">YourShoes</strong>
        <Link to="/">Home</Link>
        <Link to="/">Women</Link>
        <Link to="/">Men</Link>

        <Form>
          <input type="text" />
          <button type="button">
            <Link to="">
              <FiSearch />
            </Link>
          </button>
        </Form>

        <button type="button">
          <Link to="/user">
            <FiUser />
          </Link>
        </button>

        <button type="button">
          <Link to="/cart">
            <FiShoppingCart />
          </Link>
        </button>

        <button className="fiPower" type="button">
          <FiPower />
        </button>
      </FirstItems>
    </Container>
  );
};

export default TopBar;
