import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  position: fixed;
  top: 0px;
  z-index: 1;
  width: 100%;
  max-height: 60px;
  height: 100vh;
  background: var(--primary);
`;

export const FirstItems = styled.div`
  width: 100%;
  display: flex;
  box-shadow: 1px 3px 4px rgba(0, 0, 0, 0.17);
  background: var(--primary);

  justify-content: space-around;
  align-items: center;

  .yourShoes,
  .cartIcon,
  .searchIcon,
  .menuToggle {
    display: none;
  }

  strong {
    font-size: 2.4rem;
    line-height: 2.9rem;
  }

  button {
    display: flex;
    background-color: transparent;
    border: none;
    border-radius: 0.8rem;
    max-width: 4rem;
    max-height: 4rem;
    width: 100%;
    height: 100%;
    transition: background-color 0.2s;
    align-items: center;
    justify-content: center;

    svg {
      width: 2.4rem;
      height: 2.4rem;
    }

    a {
      text-decoration: none;

      svg {
        text-decoration: none;
        color: var(--secundary);
        width: 2.4rem;
        height: 2.4rem;
      }
    }

    &:hover {
      background-color: var(--green);
    }
  }

  .fiPower {
    max-width: 4rem;
    max-height: 4rem;
    width: 100%;
    height: 100%;
    background-color: var(--secundary);
    transition: background-color 0.2s;

    svg {
      color: var(--primary);
      width: 2.4rem;
      height: 2.4rem;
      transition: color 0.1s;

      &:hover {
        color: var(--secundary);
      }
    }

    &:hover {
      background-color: var(--green);
    }
  }

  > a {
    text-decoration: none;
    color: var(--secundary);
    font-weight: bold;
    font-size: 2.2rem;
    line-height: 2.7rem;
    transition: color 0.2s;

    :hover {
      color: var(--green);
    }
  }

  @media (max-width: 768px), (max-width: 1024px) {
    button {
      height: 70%;

      svg {
        width: 1.9rem;
        height: 1.9rem;
      }
    }

    .fiPower {
      height: 70%;

      svg {
        width: 1.9rem;
        height: 1.9rem;
      }
    }

    a {
      font-size: 1.6rem;
      line-height: 2.3rem;
    }
  }

  @media (max-width: 560px) {
    a {
      display: none;
    }

    button {
      display: none;
    }

    .yourShoes,
    .cartIcon,
    .searchIcon,
    .menuToggle {
      display: flex;
      color: var(--secundary);
    }
  }
`;

export const Form = styled.div`
  display: flex;
  background: var(--box-background);
  border-radius: 10px;
  padding: 0.8rem 2rem;
  max-width: 40rem;
  max-height: 4rem;
  height: 100%;
  width: 100%;
  justify-content: space-between;
  align-items: center;

  input {
    width: 90%;
    background-color: transparent;
    border: none;
    font-weight: normal;
    font-size: 1.6rem;
  }

  button {
    background-color: transparent;
    border: none;
    max-width: 2rem;
    max-height: 2rem;

    svg {
      width: 2rem;
      height: 2rem;
      transition: color 0.2s;

      &:hover {
        color: var(--green);
      }
    }

    &:hover {
      background-color: transparent;
    }
  }

  @media (max-width: 768px), (max-width: 1024px) {
    width: 28%;
  }

  @media (max-width: 560px) {
    display: none;
  }
`;
