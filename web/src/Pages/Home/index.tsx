import React, { useState, useEffect } from 'react';
import { FiShoppingCart } from 'react-icons/fi';
import Slider from 'react-slick';
import api from '../../services/api';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import TopBar from '../../Components/TopBar';
import { Container, Title, SaleInfo, Spotlights, ProductItem } from './styles';

interface ShoeProps {
  id: number;
  title: string;
  price: number;
  image: string;
}

const Home: React.FC = () => {
  const [shoes, setShoes] = useState<ShoeProps[]>([]);
  const [selectedProducts, setSelectedProducts] = useState<number[]>([]);

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    swipeToSlide: true,
    slidesToShow: 4,
    initialSlide: 0,
  };

  function handleAddToCart(id: number) {
    const alreadySelected = selectedProducts.findIndex(
      (product) => product === id,
    );

    if (alreadySelected >= 0) {
      const filteredProduct = selectedProducts.filter(
        (product) => product !== id,
      );

      setSelectedProducts(filteredProduct);
    } else {
      setSelectedProducts([...selectedProducts, id]);
    }
  }

  useEffect(() => {
    api.get(`products`).then((response) => {
      setShoes(response.data);
    });
  }, []);

  return (
    <>
      <TopBar />
      <Container>
        <Title>
          <h1>YourShoes</h1>
        </Title>
        <SaleInfo>
          <strong>
            50% off
            <br />
            everything
          </strong>
          <span>with code: babe50</span>
        </SaleInfo>

        <Spotlights>
          <strong>Spotlights</strong>
          <Slider {...settings} className="slider">
            {shoes.map((shoe) => (
              <ProductItem key={shoe.id}>
                <div className="itemSlider">
                  <div
                    className={
                      selectedProducts.includes(shoe.id) ? 'selected' : ''
                    }
                  >
                    <img src={shoe.image} alt={shoe.title} />
                    <footer>
                      <strong>R$ {shoe.price}</strong>
                      <button
                        onClick={() => handleAddToCart(shoe.id)}
                        type="button"
                      >
                        <FiShoppingCart />
                      </button>
                    </footer>
                  </div>
                  <strong className="shoeName">{shoe.title}</strong>
                </div>
              </ProductItem>
            ))}
          </Slider>
        </Spotlights>
      </Container>
    </>
  );
};

export default Home;
