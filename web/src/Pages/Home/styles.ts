import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media (max-width: 768px), (max-width: 1024px) {
    padding: 50px 60px;
  }

  @media (max-width: 560px) {
    padding: 30px 10px;
  }
`;

export const Title = styled.div`
  margin-top: 100px;
  h1 {
    font-size: 4.5rem;
    line-height: 5.5rem;
  }

  @media (max-width: 768px), (max-width: 1024px) {
    h1 {
      font-size: 3.8rem;
    }
  }

  @media (max-width: 560px) {
    display: none;
  }
`;

export const SaleInfo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  width: 100%;
  max-height: 180px;
  height: 100vh;
  margin-top: 5rem;

  border-radius: 20px;
  background: var(--box-background);

  strong {
    font-size: 3.2rem;
    line-height: 3.9rem;
    text-transform: uppercase;
    text-align: center;
  }

  span {
    font-size: 1.6rem;
    line-height: 2rem;
  }

  @media (max-width: 768px), (max-width: 1024px) {
    strong {
      font-size: 3rem;
    }

    span {
      font-size: 1.4rem;
    }
  }

  @media (max-width: 560px) {
    margin: 0;

    strong {
      font-size: 2.8rem;
    }

    span {
      font-size: 1.5rem;
    }
  }
`;

export const Spotlights = styled.div`
  margin-top: 38px;
  display: flex;
  width: 100%;
  flex-direction: column;

  strong {
    align-self: center;
    font-size: 3rem;
    line-height: 3.7rem;
  }

  .slider {
    display: flex;
    flex-direction: column;
    margin-top: 21px;
  }
`;

export const ProductItem = styled.div`
  .itemSlider {
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 300px;
    width: 100%;
    padding: 0 10px;

    .shoeName {
      margin-top: 17px;
      font-size: 2rem;
      line-height: 2.4rem;
    }
  }

  .itemSlider div {
    display: flex;
    width: 100%;
    flex-direction: column;
    align-items: center;
    border-radius: 10px;
    overflow: hidden;
    color: var(--primary);

    img {
      max-width: 200px;
      max-height: 200px;
      height: 100%;
      width: 100%;
    }

    footer {
      display: flex;
      max-height: 60px;
      height: 100vh;
      width: 100%;

      background: var(--boxInfo);
      justify-content: space-between;
      align-items: center;
      padding: 5px 17px;

      strong {
        font-size: 2rem;
        line-height: 2.4rem;
      }

      button {
        display: flex;
        align-items: center;
        justify-content: center;
        background: transparent;
        border: none;
        border-radius: 10px;
        max-width: 45px;
        max-height: 45px;
        width: 100vw;
        height: 100vh;

        svg {
          width: 26px;
          height: 26px;
          color: var(--primary);
        }
      }
    }
  }

  .itemSlider div.selected {
    footer {
      background: #bfffe6;

      strong {
        color: var(--secundary);
      }
      button {
        background: var(--green);

        svg {
          color: var(--secundary);
        }
      }
    }
  }
`;
