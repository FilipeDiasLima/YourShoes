import React from 'react';
// import { Link } from 'react-router-dom';
import { FiPlus, FiMinus } from 'react-icons/fi';

import TopBar from '../../Components/TopBar';

import { Container, CartItems, CartItem, Total } from './styles';

const Cart: React.FC = () => {
  return (
    <>
      <TopBar />
      <Container>
        <CartItems>
          <CartItem>
            <div className="infoItem">
              <img
                src="https://rocketseat-cdn.s3-sa-east-1.amazonaws.com/modulo-redux/tenis1.jpg"
                alt="Tenis"
              />
              <strong>Black Shoe</strong>
              <span>R$ 49,90</span>
            </div>
            <div className="amoutAndSubtotalItem">
              <strong>Amount</strong>
              <div className="amount">
                <button className="minus" type="button">
                  <FiMinus />
                </button>
                <input type="text" readOnly value="1" />
                <button className="plus" type="button">
                  <FiPlus />
                </button>
              </div>
              <strong className="subtotal">Subtotal</strong>
              <span>R$ 49,90</span>
            </div>
          </CartItem>
        </CartItems>
        <Total>
          <strong>Total</strong>
          <strong>R$ 200,00</strong>
        </Total>
      </Container>
    </>
  );
};

export default Cart;
