import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-top: 150px;
`;

export const CartItems = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 600px;
  width: 100%;
  background: var(--box-background);
  border-radius: 10px;
  padding: 25px 20px;
  justify-content: center;
`;

export const CartItem = styled.div`
  display: flex;
  flex-direction: row;
  padding: 15px 20px;
  align-items: center;
  justify-content: space-between;

  .infoItem {
    display: flex;
    flex-direction: column;

    img {
      max-width: 200px;
      border-radius: 10px;
    }

    > strong {
      margin-top: 7px;
      font-size: 1.8rem;
      line-height: 2.2rem;
    }

    > span {
      margin-top: 12px;
      font-size: 2rem;
      line-height: 2.4rem;
    }
  }
  .amoutAndSubtotalItem {
    display: flex;
    flex-direction: column;
    justify-content: flex-end;

    > strong {
      font-size: 2rem;
      line-height: 2.4rem;
      text-align: right;
    }

    .subtotal {
      margin-top: 15px;
      font-size: 2.5rem;
      text-align: right;
    }

    > span {
      margin-top: 10px;
      font-size: 2rem;
      line-height: 2.4rem;
      text-align: right;
    }

    .amount {
      display: flex;
      flex-direction: row;
      margin-top: 15px;

      .minus {
        width: 50px;
        height: 50px;
        background: var(--green);
        border: none;
        border-top-left-radius: 10px;
        border-bottom-left-radius: 10px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;

        svg {
          width: 24px;
          height: 24px;
        }
      }

      > input {
        padding: 13px;
        text-align: center;
        max-width: 60px;
        max-height: 50px;
        width: 100vw;
        height: 100vh;
        border: none;
        border-radius: 0;
        font-weight: bold;
      }

      .plus {
        width: 50px;
        height: 50px;
        background: var(--green);
        border: none;
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;

        svg {
          width: 24px;
          height: 24px;
        }
      }
    }
  }
`;

export const Total = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 600px;
  max-height: 85px;
  height: 100%;
  width: 100%;
  background: var(--box-background);
  border-radius: 10px;
  padding: 20px 40px;
  align-items: center;
  justify-content: space-between;

  > strong {
    font-size: 3rem;
    line-height: 3.7rem;

    & + strong {
      font-size: 2.8rem;
      line-height: 3.7rem;
    }
  }
`;
